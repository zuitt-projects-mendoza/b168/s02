<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S02: Activity</title>
</head>
<body>

	<h2>Divisibles of Five</h2>

	<?php modifiedForLoop(); ?>

	<h2>Array Manipulation</h2>

	<ul>
		<?php foreach($studentNames as $student) { ?>
			<li><?php echo $student; ?></li>
		<?php } ?>
	</ul>

	<?php array_push($studentNames, 'John Smith') ?>

	<pre><?php print_r($studentNames); ?></pre>

	<pre><?php echo count($studentNames); ?></pre>

	<?php array_push($studentNames, 'Jane Smith') ?>

	<pre><?php print_r($studentNames); ?></pre>

	<pre><?php echo count($studentNames); ?></pre>

	<?php array_shift($studentNames); ?>

	<pre><?php print_r($studentNames); ?></pre>

	<pre><?php echo count($studentNames); ?></pre>


	